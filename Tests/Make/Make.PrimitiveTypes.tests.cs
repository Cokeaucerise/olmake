using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
  public partial class MakeTests
  {
    [Fact]
    public void MakeA_Char_ReturnRandomChar()
    {
      //Given

      //When
      char c = Make.A<char>();

      //Then
      c.Should().NotBeNull();
      c.GetType().Should().Be<char>();
      c.Should().BeGreaterOrEqualTo(char.MinValue);
      c.Should().BeLessOrEqualTo(char.MaxValue);
    }

    [Fact]
    public void MakeA_String_ReturnRandomWord()
    {
      //Given

      //When
      string s = Make.A<string>();

      //Then
      s.Should().NotBeNull();
      s.GetType().Should().Be<string>();
      s.Count().Should().BeGreaterThan(0);
      s.Split(' ').Count().Should().Be(1);
    }

    [Fact]
    public void MakeA_Boolean_ReturnRandomBoolean()
    {
      //Given

      //When
      bool b = Make.A<bool>();

      //Then
      b.GetType().Should().Be<Boolean>();
    }

    [Fact]
    public void MakeA_Byte_ReturnRandomByte()
    {
      //Given

      //When
      Byte b = Make.A<Byte>();

      //Then
      b.GetType().Should().Be<byte>();
      b.GetType().Should().Be<Byte>();
      b.Should().BeGreaterOrEqualTo(Byte.MinValue);
      b.Should().BeLessOrEqualTo(Byte.MaxValue);
    }

    [Fact]
    public void MakeA_Int16_ReturnRandomInt16()
    {
      //Given

      //When
      Int16 int16 = Make.A<Int16>();

      //Then
      int16.GetType().Should().Be<short>();
      int16.GetType().Should().Be<Int16>();
      int16.Should().BeGreaterOrEqualTo(Int16.MinValue);
      int16.Should().BeLessOrEqualTo(Int16.MaxValue);
    }

    [Fact]
    public void MakeA_Int32_ReturnRandomInt32()
    {
      //Given

      //When
      Int32 int32 = Make.A<Int32>();

      //Then
      int32.GetType().Should().Be<int>();
      int32.GetType().Should().Be<Int32>();
      int32.Should().BeGreaterOrEqualTo(Int32.MinValue);
      int32.Should().BeLessOrEqualTo(Int32.MaxValue);
    }

    [Fact]
    public void MakeA_Int64_ReturnRandomInt64()
    {
      //Given

      //When
      Int64 int64 = Make.A<Int64>();

      //Then
      int64.GetType().Should().Be<long>();
      int64.GetType().Should().Be<Int64>();
      int64.Should().BeGreaterOrEqualTo(Int64.MinValue);
      int64.Should().BeLessOrEqualTo(Int64.MaxValue);
    }

    [Fact]
    public void MakeA_IntPtr_ReturnRandomIntPtr()
    {
      //Given

      //When
      IntPtr intPtr = Make.A<IntPtr>();

      //Then
      intPtr.Should().NotBeNull();
      intPtr.GetType().Should().Be<IntPtr>();
    }

    [Fact]
    public void MakeA_SByte_ReturnRandomSByte()
    {
      //Given

      //When
      SByte b = Make.A<SByte>();

      //Then
      b.GetType().Should().Be<sbyte>();
      b.GetType().Should().Be<SByte>();
      b.Should().BeGreaterOrEqualTo(SByte.MinValue);
      b.Should().BeLessOrEqualTo(SByte.MaxValue);
    }

    [Fact]
    public void MakeA_UInt16_ReturnRandomUInt16()
    {
      //Given

      //When
      UInt16 uInt16 = Make.A<UInt16>();

      //Then
      uInt16.GetType().Should().Be<ushort>();
      uInt16.GetType().Should().Be<UInt16>();
      uInt16.Should().BeGreaterOrEqualTo(UInt16.MinValue);
      uInt16.Should().BeLessOrEqualTo(UInt16.MaxValue);
    }

    [Fact]
    public void MakeA_UInt32_ReturnRandomUInt32()
    {
      //Given

      //When
      UInt32 uInt32 = Make.A<UInt32>();

      //Then
      uInt32.GetType().Should().Be<uint>();
      uInt32.GetType().Should().Be<UInt32>();
      uInt32.Should().BeGreaterOrEqualTo(UInt32.MinValue);
      uInt32.Should().BeLessOrEqualTo(UInt32.MaxValue);
    }

    [Fact]
    public void MakeA_UInt64_ReturnRandomUInt64()
    {
      //Given

      //When
      UInt64 uInt64 = Make.A<UInt64>();

      //Then
      uInt64.GetType().Should().Be<ulong>();
      uInt64.GetType().Should().Be<UInt64>();
      uInt64.Should().BeGreaterOrEqualTo(UInt64.MinValue);
      uInt64.Should().BeLessOrEqualTo(UInt64.MaxValue);
    }

    [Fact]
    public void MakeA_UIntPtr_ReturnRandomUIntPtr()
    {
      //Given

      //When
      UIntPtr uIntPtr = Make.A<UIntPtr>();

      //Then
      uIntPtr.Should().NotBeNull();
      uIntPtr.GetType().Should().Be<UIntPtr>();
    }

    [Fact]
    public void MakeA_Float_ReturnRandomFloat()
    {
      //Given

      //When
      float f = Make.A<float>();

      //Then
      f.GetType().Should().Be<float>();
      f.GetType().Should().Be<Single>();
      f.Should().BeGreaterOrEqualTo(float.MinValue);
      f.Should().BeLessOrEqualTo(float.MaxValue);
    }

    [Fact]
    public void MakeA_Double_ReturnRandomDouble()
    {
      //Given

      //When
      double d = Make.A<double>();

      //Then
      d.GetType().Should().Be<double>();
      d.GetType().Should().Be<Double>();
      d.Should().BeGreaterOrEqualTo(double.MinValue);
      d.Should().BeLessOrEqualTo(double.MaxValue);
    }

    [Fact]
    public void MakeA_Decimal_ReturnRandomDecimal()
    {
      //Given

      //When
      decimal d = Make.A<decimal>();

      //Then
      d.GetType().Should().Be<decimal>();
      d.GetType().Should().Be<Decimal>();
      d.Should().BeGreaterOrEqualTo(decimal.MinValue);
      d.Should().BeLessOrEqualTo(decimal.MaxValue);
    }

  }
}