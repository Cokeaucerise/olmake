using System;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
  public partial class MakeTests
  {
    [Fact]
    public void MakeA_EmptyObject_FillPublicChar()
    {
      //Given

      //When
      PublicFields<char> tc = Make.A<PublicFields<char>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<char>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(char.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(char.MaxValue);

      PublicFields<char>.PublicStaticGetSet.GetType().Should().Be<char>();
      PublicFields<char>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(char.MinValue);
      PublicFields<char>.PublicStaticGetSet.Should().BeLessOrEqualTo(char.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicString()
    {
      //Given

      //When
      PublicFields<string> tc = Make.A<PublicFields<string>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<string>();
      tc.PublicGetSet.Should().NotBeNull();

      PublicFields<string>.PublicStaticGetSet.GetType().Should().Be<string>();
      PublicFields<string>.PublicStaticGetSet.Should().NotBeNull();
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicBoolean()
    {
      //Given

      //When
      PublicFields<Boolean> tc = Make.A<PublicFields<Boolean>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<Boolean>();

      PublicFields<Boolean>.PublicStaticGetSet.GetType().Should().Be<Boolean>();
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicByte()
    {
      //Given

      //When
      PublicFields<Byte> tc = Make.A<PublicFields<Byte>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<Byte>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(Byte.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(Byte.MaxValue);

      PublicFields<Byte>.PublicStaticGetSet.GetType().Should().Be<Byte>();
      PublicFields<Byte>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(Byte.MinValue);
      PublicFields<Byte>.PublicStaticGetSet.Should().BeLessOrEqualTo(Byte.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicInt16()
    {
      //Given

      //When
      PublicFields<Int16> tc = Make.A<PublicFields<Int16>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<Int16>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(Int16.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(Int16.MaxValue);

      PublicFields<Int16>.PublicStaticGetSet.GetType().Should().Be<Int16>();
      PublicFields<Int16>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(Int16.MinValue);
      PublicFields<Int16>.PublicStaticGetSet.Should().BeLessOrEqualTo(Int16.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicInt32()
    {
      //Given

      //When
      PublicFields<Int32> tc = Make.A<PublicFields<Int32>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<Int32>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(Int32.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(Int32.MaxValue);

      PublicFields<Int32>.PublicStaticGetSet.GetType().Should().Be<Int32>();
      PublicFields<Int32>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(Int32.MinValue);
      PublicFields<Int32>.PublicStaticGetSet.Should().BeLessOrEqualTo(Int32.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicInt64()
    {
      //Given

      //When
      PublicFields<Int64> tc = Make.A<PublicFields<Int64>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<Int64>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(Int64.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(Int64.MaxValue);

      PublicFields<Int64>.PublicStaticGetSet.GetType().Should().Be<Int64>();
      PublicFields<Int64>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(Int64.MinValue);
      PublicFields<Int64>.PublicStaticGetSet.Should().BeLessOrEqualTo(Int64.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicIntPtr()
    {
      //Given

      //When
      PublicFields<IntPtr> tc = Make.A<PublicFields<IntPtr>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<IntPtr>();
      PublicFields<IntPtr>.PublicStaticGetSet.GetType().Should().Be<IntPtr>();
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicSByte()
    {
      //Given

      //When
      PublicFields<SByte> tc = Make.A<PublicFields<SByte>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<SByte>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(SByte.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(SByte.MaxValue);

      PublicFields<SByte>.PublicStaticGetSet.GetType().Should().Be<SByte>();
      PublicFields<SByte>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(SByte.MinValue);
      PublicFields<SByte>.PublicStaticGetSet.Should().BeLessOrEqualTo(SByte.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicUInt16()
    {
      //Given

      //When
      PublicFields<UInt16> tc = Make.A<PublicFields<UInt16>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<UInt16>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(UInt16.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(UInt16.MaxValue);

      PublicFields<UInt16>.PublicStaticGetSet.GetType().Should().Be<UInt16>();
      PublicFields<UInt16>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(UInt16.MinValue);
      PublicFields<UInt16>.PublicStaticGetSet.Should().BeLessOrEqualTo(UInt16.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicUInt32()
    {
      //Given

      //When
      PublicFields<UInt32> tc = Make.A<PublicFields<UInt32>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<UInt32>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(UInt32.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(UInt32.MaxValue);

      PublicFields<UInt32>.PublicStaticGetSet.GetType().Should().Be<UInt32>();
      PublicFields<UInt32>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(UInt32.MinValue);
      PublicFields<UInt32>.PublicStaticGetSet.Should().BeLessOrEqualTo(UInt32.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicUInt64()
    {
      //Given

      //When
      PublicFields<UInt64> tc = Make.A<PublicFields<UInt64>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<UInt64>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(UInt64.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(UInt64.MaxValue);

      PublicFields<UInt64>.PublicStaticGetSet.GetType().Should().Be<UInt64>();
      PublicFields<UInt64>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(UInt64.MinValue);
      PublicFields<UInt64>.PublicStaticGetSet.Should().BeLessOrEqualTo(UInt64.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicUIntPtr()
    {
      //Given

      //When
      PublicFields<UIntPtr> tc = Make.A<PublicFields<UIntPtr>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<UIntPtr>();
      PublicFields<UIntPtr>.PublicStaticGetSet.GetType().Should().Be<UIntPtr>();
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicFloat()
    {
      //Given

      //When
      PublicFields<float> tc = Make.A<PublicFields<float>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<float>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(float.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(float.MaxValue);

      PublicFields<float>.PublicStaticGetSet.GetType().Should().Be<float>();
      PublicFields<float>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(float.MinValue);
      PublicFields<float>.PublicStaticGetSet.Should().BeLessOrEqualTo(float.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicDouble()
    {
      //Given

      //When
      PublicFields<double> tc = Make.A<PublicFields<double>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<double>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(double.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(double.MaxValue);

      PublicFields<double>.PublicStaticGetSet.GetType().Should().Be<double>();
      PublicFields<double>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(double.MinValue);
      PublicFields<double>.PublicStaticGetSet.Should().BeLessOrEqualTo(double.MaxValue);
    }

    [Fact]
    public void MakeA_EmptyObject_FillPublicDecimal()
    {
      //Given

      //When
      PublicFields<decimal> tc = Make.A<PublicFields<decimal>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGetSet.GetType().Should().Be<decimal>();
      tc.PublicGetSet.Should().BeGreaterOrEqualTo(decimal.MinValue);
      tc.PublicGetSet.Should().BeLessOrEqualTo(decimal.MaxValue);

      PublicFields<decimal>.PublicStaticGetSet.GetType().Should().Be<decimal>();
      PublicFields<decimal>.PublicStaticGetSet.Should().BeGreaterOrEqualTo(decimal.MinValue);
      PublicFields<decimal>.PublicStaticGetSet.Should().BeLessOrEqualTo(decimal.MaxValue);
    }

    //===================================================================//

    [Fact]
    public void MakeA_EmptyObject_NotFillReadonlyFields()
    {
      //Given

      //When
      PublicReadonlys<object> tc = Make.A<PublicReadonlys<object>>();

      //Then
      tc.Should().NotBeNull();

      tc.PublicGet.Should().Be(default(object));
      tc.PublicReadonly.Should().Be(default(object));
      PublicReadonlys<object>.PublicStaticGet.Should().Be(default(object));
      PublicReadonlys<object>.PublicStaticReadonly.Should().Be(default(object));
    }
  }
}