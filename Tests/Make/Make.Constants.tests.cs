using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{

  public partial class MakeTests
  {
    [Fact]
    public void MakeA_EmptyObject_NotFillConstantBoolean()
    {
      //Given

      //When
      Constant constants = Make.A<Constant>();

      //Then
      constants.Should().NotBeNull();

      Constant.PUBLIC_CONSTANT.Should().Be("string");

      IEnumerable<string> strings = GetPrivateFieldsOfType<string>(constants);

      strings.Count().Should().Be(1);
      foreach (string s in strings)
      {
        s.Should().Be("string");
      }
    }
  }
}