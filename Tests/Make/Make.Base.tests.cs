using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OLMake.Tests
{
	public partial class MakeTests
	{
		internal class AllFields
		{
			public char fChar { get; set; }
			public string fString { get; set; }
			public Boolean fBoolean { get; set; }
			public Byte fByte { get; set; }
			public Int16 fInt16 { get; set; }
			public Int32 fInt32 { get; set; }
			public Int64 fInt64 { get; set; }
			public IntPtr fIntPtr { get; set; }
			public SByte fSByte { get; set; }
			public UInt16 fUInt16 { get; set; }
			public UInt32 fUInt32 { get; set; }
			public UInt64 fUInt64 { get; set; }
			public UIntPtr fUIntPtr { get; set; }
			public float fFloat { get; set; }
			public double fDouble { get; set; }
			public decimal fDecimal { get; set; }
		}

		public record RecordAllFields
		{
			public char fChar { get; init; }
			public string fString { get; init; }
			public Boolean fBoolean { get; init; }
			public Byte fByte { get; init; }
			public Int16 fInt16 { get; init; }
			public Int32 fInt32 { get; init; }
			public Int64 fInt64 { get; init; }
			public IntPtr fIntPtr { get; init; }
			public SByte fSByte { get; init; }
			public UInt16 fUInt16 { get; init; }
			public UInt32 fUInt32 { get; init; }
			public UInt64 fUInt64 { get; init; }
			public UIntPtr fUIntPtr { get; init; }
			public float fFloat { get; init; }
			public double fDouble { get; init; }
			public decimal fDecimal { get; init; }
		}

		internal class InheritAllFields : AllFields
		{
			public string Name { get; set; }
		}

		internal class GenericInnerClass<T>
		{
			public T GenericType { get; set; }
		}

		internal class ParameterConstructor
		{
			public string[] strings { get; }

			public ParameterConstructor(string[] param)
			{
				this.strings = param;
			}
		}

		internal record ParameterConstructorRecord
		{
			public ParameterConstructorRecord(object a, object b)
			{
				this.a = a;
				this.b = b;
			}

			public object a { get; init; }
			public object b { get; init; }
		}

		internal class Constant
		{
			public const string PUBLIC_CONSTANT = "string";
			private const string PRIVATE_CONSTANT = "string";
		}

		internal class PublicFields<T>
		{
			public T PublicGetSet { get; set; }

			public static T PublicStaticGetSet { get; set; }
		}

		internal class PublicReadonlys<T>
		{
			public T PublicGet { get; }
			public readonly T PublicReadonly;

			public static T PublicStaticGet { get; }
			public static readonly T PublicStaticReadonly;
		}

		internal class PrivateFields<T>
		{
			private T PrivateGetSet { get; set; }
			private static T PrivateStaticGetSet { get; set; }
		}

		internal class PrivateReadonlys<T>
		{
			private T PrivateGet { get; }
			private readonly T PrivateReadonly = default(T);

			private static T PrivateStaticGet { get; }
			private static readonly T PrivateStaticReadonly = default(T);
		}

		protected IEnumerable<T> GetPrivateFieldsOfType<T>(object instance)
		{
			return instance.GetType()
					.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic)
					.Where(f => f.FieldType == typeof(T))
					.Select(f => (T)f.GetValue(instance));
		}

	}
}