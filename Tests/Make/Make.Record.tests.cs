using System;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
	public partial class MakeTests
	{

		[Fact]
		public void MakeA_EmptyRecord_NewRecordWithoutDefaultValues()
		{
			//Given

			//When
			RecordAllFields af = Make.A<RecordAllFields>();

			//Then
			af.fChar.Should().NotBeNull();
			af.fString.Should().Be(default(string));
			af.fBoolean.GetType().Should().Be<Boolean>();
			af.fByte.GetType().Should().Be<Byte>();
			af.fInt16.GetType().Should().Be<Int16>();
			af.fInt32.GetType().Should().Be<Int32>();
			af.fInt64.GetType().Should().Be<Int64>();
			af.fIntPtr.GetType().Should().Be<IntPtr>();
			af.fSByte.GetType().Should().Be<SByte>();
			af.fUInt16.GetType().Should().Be<UInt16>();
			af.fUInt32.GetType().Should().Be<UInt32>();
			af.fUInt64.GetType().Should().Be<UInt64>();
			af.fUIntPtr.GetType().Should().Be<UIntPtr>();
			af.fFloat.GetType().Should().Be<float>();
			af.fDouble.GetType().Should().Be<double>();
			af.fDecimal.GetType().Should().Be<decimal>();
		}

		[Fact]
		public void MakeA_ParameterConstructorRecord_ReturnNull()
		{
			//Given

			//When
			ParameterConstructorRecord obj = Make.A<ParameterConstructorRecord>();

			//Then
			obj.Should().BeNull();
		}

		[Fact]
		public void MakeA_ParameterConstructorRecordWithParam_NewObjectWithParams()
		{
			//Given
			object a = new { field = "testa" };
			object b = new { field = "test b" };

			//When
			ParameterConstructorRecord obj = Make.A<ParameterConstructorRecord>(a, b);

			//Then
			obj.a.Should().BeEquivalentTo(a);
			obj.b.Should().BeEquivalentTo(b);
		}
	}
}