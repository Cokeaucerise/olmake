using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
  public partial class MakeTests
  {
    [Fact]
    public void MakeAList_ListOfBaseType_NotNullIEnumerable()
    {
      //Given

      //When
      IEnumerable<string> strings = Make.AList<string>();

      //Then
      strings.Should().NotBeNull();
    }

    [Theory]
    [InlineData(25)]
    public void MakeAList_ListOfDefaultCount_CountIsCorrect(int count)
    {
      //Given

      //When
      IEnumerable<string> strings = Make.AList<string>();

      //Then
      strings.Should().NotBeNull();
      strings.Count().Should().Be(count);
    }

    [Theory]
    [InlineData(1)]
    [InlineData(5)]
    [InlineData(10)]
    [InlineData(25)]
    [InlineData(50)]
    [InlineData(100)]
    [InlineData(1000)]
    [InlineData(10000)]
    [InlineData(100000)]
    public void MakeAList_ListOfSpecificCount_CountIsCorrect(int count)
    {
      //Given

      //When
      IEnumerable<string> strings = Make.AList<string>(count);

      //Then
      strings.Should().NotBeNull();
      strings.Count().Should().Be(count);
    }

    [Fact]
    public void MakeAList_ListOfObject_ItemAreNotDefault()
    {
      //Given

      //When
      IEnumerable<object> coll = Make.AList<object>();

      //Then
      coll.Should().NotBeNull();

      foreach (object item in coll)
      {
        item.Should().NotBe(default(object));
      }
    }

    [Fact]
    public void MakeA_NestedEmptyConstructorObject_InnerObjectIsNotNull()
    {
      //Given

      //When
      GenericInnerClass<List<AllFields>> nestedColl = Make.A<GenericInnerClass<List<AllFields>>>();

      //Then
      nestedColl.Should().NotBeNull();
      nestedColl.GenericType.Should().NotBeNull();
    }

    [Fact]
    public void MakeAList_WithFactory_NewObjectWithCorrectvalue()
    {
      //Given
      Func<AllFields> factory = () => new AllFields
      {
        fChar = 'a',
        fString = "string",
        fBoolean = true,
        fByte = new Byte(),
        fInt16 = 1,
        fInt32 = 2,
        fInt64 = 3,
        fIntPtr = (IntPtr) 4,
        fSByte = new SByte(),
        fUInt16 = 1,
        fUInt32 = 2,
        fUInt64 = 3,
        fUIntPtr = (UIntPtr) 4,
        fFloat = 1,
        fDouble = 0.1,
        fDecimal = 1,
      };

      //When
      IEnumerable<AllFields> afc = Make.AList<AllFields>(factory);

      //Then
      foreach (AllFields af in afc)
      {
        af.fChar.Should().Be('a');
        af.fString.Should().Be("string");
        af.fBoolean.Should().Be(true);
        af.fByte.Should().Be(new Byte());
        af.fInt16.Should().Be(1);
        af.fInt32.Should().Be(2);
        af.fInt64.Should().Be(3);
        af.fIntPtr.Should().Be((IntPtr) 4);
        af.fSByte.Should().Be(new SByte());
        af.fUInt16.Should().Be(1);
        af.fUInt32.Should().Be(2);
        af.fUInt64.Should().Be(3);
        af.fUIntPtr.Should().Be((UIntPtr) 4);
        af.fFloat.Should().Be(1);
        af.fDouble.Should().Be(0.1);
        af.fDecimal.Should().Be(1);
      }
    }
  }
}