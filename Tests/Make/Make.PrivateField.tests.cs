using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
  public partial class MakeTests
  {
    [Fact]
    public void MakeA_EmptyObject_FillPrivateChar()
    {
      //Given

      //When
      PrivateFields<char> tc = Make.A<PrivateFields<char>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<char> chars = GetPrivateFieldsOfType<char>(tc);

      chars.Count().Should().Be(2);

      foreach (char c in chars)
      {
        c.Should().NotBeNull();
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateString()
    {
      //Given

      //When
      PrivateFields<string> tc = Make.A<PrivateFields<string>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<string> strings = GetPrivateFieldsOfType<string>(tc);

      strings.Count().Should().Be(2);

      foreach (string s in strings)
      {
        s.Should().NotBeNull();
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateInt16()
    {
      //Given

      //When
      PrivateFields<Int16> tc = Make.A<PrivateFields<Int16>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<Int16> int16s = GetPrivateFieldsOfType<Int16>(tc);

      int16s.Count().Should().Be(2);

      foreach (Int16 s in int16s)
      {
        s.Should().BeGreaterOrEqualTo(Int16.MinValue);
        s.Should().BeLessOrEqualTo(Int16.MaxValue);
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateInt32()
    {
      //Given

      //When
      PrivateFields<Int32> tc = Make.A<PrivateFields<Int32>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<Int32> int32s = GetPrivateFieldsOfType<Int32>(tc);

      int32s.Count().Should().Be(2);

      foreach (Int32 s in int32s)
      {
        s.Should().BeGreaterOrEqualTo(Int32.MinValue);
        s.Should().BeLessOrEqualTo(Int32.MaxValue);
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateInt64()
    {
      //Given

      //When
      PrivateFields<Int64> tc = Make.A<PrivateFields<Int64>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<Int64> int64s = GetPrivateFieldsOfType<Int64>(tc);

      int64s.Count().Should().Be(2);

      foreach (Int64 s in int64s)
      {
        s.Should().BeGreaterOrEqualTo(Int64.MinValue);
        s.Should().BeLessOrEqualTo(Int64.MaxValue);
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateFloat()
    {
      //Given

      //When
      PrivateFields<float> tc = Make.A<PrivateFields<float>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<float> floats = GetPrivateFieldsOfType<float>(tc);

      floats.Count().Should().Be(2);

      foreach (float s in floats)
      {
        s.Should().BeGreaterOrEqualTo(float.MinValue);
        s.Should().BeLessOrEqualTo(float.MaxValue);
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateDouble()
    {
      //Given

      //When
      PrivateFields<double> tc = Make.A<PrivateFields<double>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<double> doubles = GetPrivateFieldsOfType<double>(tc);

      doubles.Count().Should().Be(2);

      foreach (double s in doubles)
      {
        s.Should().BeGreaterOrEqualTo(double.MinValue);
        s.Should().BeLessOrEqualTo(double.MaxValue);
      }
    }

    [Fact]
    public void MakeA_EmptyObject_FillPrivateDecimal()
    {
      //Given

      //When
      PrivateFields<decimal> tc = Make.A<PrivateFields<decimal>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<decimal> decimals = GetPrivateFieldsOfType<decimal>(tc);

      decimals.Count().Should().Be(2);

      foreach (decimal s in decimals)
      {
        s.Should().BeGreaterOrEqualTo(decimal.MinValue);
        s.Should().BeLessOrEqualTo(decimal.MaxValue);
      }
    }

    //=====================================================================//

    [Fact]
    public void MakeA_EmptyObject_NotFillPrivateReadonlyObject()
    {
      //Given

      //When
      PrivateReadonlys<object> tc = Make.A<PrivateReadonlys<object>>();

      //Then
      tc.Should().NotBeNull();

      IEnumerable<object> objects = GetPrivateFieldsOfType<object>(tc);

      objects.Count().Should().Be(4);

      foreach (object c in objects)
      {
        c.Should().Be(default(object));
      }
    }

  }
}