using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
  public partial class MakeTests
  {
    [Fact]
    public void MakeAWord__GiveBackAWord()
    {
      //Given

      //When
      string word = Make.AWord();

      //Then
      word.Should().NotContain(".", " ");
    }

    [Theory]
    [InlineData(8, 16)]
    public void MakeASentence__GiveBackSentence(int min, int max)
    {
      //Given

      //When
      string sentence = Make.ASentence();

      //Then
      sentence.First().Should().BeGreaterOrEqualTo('A');
      sentence.First().Should().BeLessOrEqualTo('Z');
      sentence.Should().EndWith(".");

      sentence.Split(' ').Count().Should().BeGreaterOrEqualTo(min);
      sentence.Split(' ').Count().Should().BeLessOrEqualTo(max);
    }

    [Theory]
    [InlineData(5, 10)]
    public void MakeAParagraph__GiveBackParagraph(int min, int max)
    {
      //Given

      //When
      string paragraph = Make.AParagraph();

      //Then
      paragraph.First().Should().BeGreaterOrEqualTo('A');
      paragraph.First().Should().BeLessOrEqualTo('Z');
      paragraph.Should().EndWith(".");

      IEnumerable<string> sentences = paragraph.Split('.').Where(s => !string.IsNullOrEmpty(s));
      sentences.Count().Should().BeGreaterOrEqualTo(min);
      sentences.Count().Should().BeLessOrEqualTo(max);
    }

    [Theory]
    [InlineData(3, 6)]
    public void MakeAText__GiveBackText(int min, int max)
    {
      //Given

      //When
      string text = Make.AText();

      //Then
      text.First().Should().BeGreaterOrEqualTo('A');
      text.First().Should().BeLessOrEqualTo('Z');
      text.Should().EndWith(".");

      IEnumerable<string> paragraphs = text.Split('\r').Where(s => !string.IsNullOrEmpty(s));
      paragraphs.Count().Should().BeGreaterOrEqualTo(min);
      paragraphs.Count().Should().BeLessOrEqualTo(max);

      for (int i = 1; i < paragraphs.Count(); i++)
      {
        paragraphs.ElementAt(1).Should().StartWith("\n\n");
      }

      foreach (string paragraph in paragraphs)
      {
        paragraph.Split('.').Where(s => !string.IsNullOrEmpty(s)).Count()
          .Should().BeGreaterThan(1);
      }
    }
  }
}