using System;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
  public partial class MakeTests
  {
    [Fact]
    public void MakeA_EmptyObject_NewObjectWithoutNull()
    {
      //Given

      //When
      AllFields af = Make.A<AllFields>();

      //Then
      af.fChar.Should().NotBeNull();
      af.fString.Should().NotBeNull();
      af.fBoolean.GetType().Should().Be<Boolean>();
      af.fByte.GetType().Should().Be<Byte>();
      af.fInt16.GetType().Should().Be<Int16>();
      af.fInt32.GetType().Should().Be<Int32>();
      af.fInt64.GetType().Should().Be<Int64>();
      af.fIntPtr.GetType().Should().Be<IntPtr>();
      af.fSByte.GetType().Should().Be<SByte>();
      af.fUInt16.GetType().Should().Be<UInt16>();
      af.fUInt32.GetType().Should().Be<UInt32>();
      af.fUInt64.GetType().Should().Be<UInt64>();
      af.fUIntPtr.GetType().Should().Be<UIntPtr>();
      af.fFloat.GetType().Should().Be<float>();
      af.fDouble.GetType().Should().Be<double>();
      af.fDecimal.GetType().Should().Be<decimal>();
    }

    [Fact]
    public void MakeA_NestedClass_NewObjectWithoutNull()
    {
      //Given

      //When
      GenericInnerClass<AllFields> nc = Make.A<GenericInnerClass<AllFields>>();

      //Then
      nc.GenericType.Should().NotBeNull();

      AllFields af = nc.GenericType;
      af.fChar.Should().NotBeNull();
      af.fString.Should().NotBeNull();
      af.fBoolean.GetType().Should().Be<Boolean>();
      af.fByte.GetType().Should().Be<Byte>();
      af.fInt16.GetType().Should().Be<Int16>();
      af.fInt32.GetType().Should().Be<Int32>();
      af.fInt64.GetType().Should().Be<Int64>();
      af.fIntPtr.GetType().Should().Be<IntPtr>();
      af.fSByte.GetType().Should().Be<SByte>();
      af.fUInt16.GetType().Should().Be<UInt16>();
      af.fUInt32.GetType().Should().Be<UInt32>();
      af.fUInt64.GetType().Should().Be<UInt64>();
      af.fUIntPtr.GetType().Should().Be<UIntPtr>();
      af.fFloat.GetType().Should().Be<float>();
      af.fDouble.GetType().Should().Be<double>();
      af.fDecimal.GetType().Should().Be<decimal>();
    }

    [Fact]
    public void MakeA_GenericClass_NewObjectWithCorrectType()
    {
      //Given

      //When
      GenericInnerClass<AllFields> gc = Make.A<GenericInnerClass<AllFields>>();

      //Then
      gc.GenericType.Should().NotBeNull();
      gc.GenericType.GetType().Should().Be<AllFields>();

      AllFields af = gc.GenericType;
      af.fChar.Should().NotBeNull();
      af.fString.Should().NotBeNull();
      af.fBoolean.GetType().Should().Be<Boolean>();
      af.fByte.GetType().Should().Be<Byte>();
      af.fInt16.GetType().Should().Be<Int16>();
      af.fInt32.GetType().Should().Be<Int32>();
      af.fInt64.GetType().Should().Be<Int64>();
      af.fIntPtr.GetType().Should().Be<IntPtr>();
      af.fSByte.GetType().Should().Be<SByte>();
      af.fUInt16.GetType().Should().Be<UInt16>();
      af.fUInt32.GetType().Should().Be<UInt32>();
      af.fUInt64.GetType().Should().Be<UInt64>();
      af.fUIntPtr.GetType().Should().Be<UIntPtr>();
      af.fFloat.GetType().Should().Be<float>();
      af.fDouble.GetType().Should().Be<double>();
      af.fDecimal.GetType().Should().Be<decimal>();
    }

    [Fact]
    public void MakeA_InheritClass_NewObjectWithValues()
    {
      //Given

      //When
      InheritAllFields iaf = Make.A<InheritAllFields>();

      //Then
      iaf.Should().NotBeNull();
      iaf.GetType().Should().Be<InheritAllFields>();

      iaf.Name.Should().NotBeNull();
      iaf.fChar.Should().NotBeNull();
      iaf.fString.Should().NotBeNull();
      iaf.fBoolean.GetType().Should().Be<Boolean>();
      iaf.fByte.GetType().Should().Be<Byte>();
      iaf.fInt16.GetType().Should().Be<Int16>();
      iaf.fInt32.GetType().Should().Be<Int32>();
      iaf.fInt64.GetType().Should().Be<Int64>();
      iaf.fIntPtr.GetType().Should().Be<IntPtr>();
      iaf.fSByte.GetType().Should().Be<SByte>();
      iaf.fUInt16.GetType().Should().Be<UInt16>();
      iaf.fUInt32.GetType().Should().Be<UInt32>();
      iaf.fUInt64.GetType().Should().Be<UInt64>();
      iaf.fUIntPtr.GetType().Should().Be<UIntPtr>();
      iaf.fFloat.GetType().Should().Be<float>();
      iaf.fDouble.GetType().Should().Be<double>();
      iaf.fDecimal.GetType().Should().Be<decimal>();
    }

    [Fact]
    public void MakeA_ParameterConstructor_ReturnNull()
    {
      //Given

      //When
      ParameterConstructor obj = Make.A<ParameterConstructor>();

      //Then
      obj.Should().BeNull();
    }

    [Fact]
    public void MakeA_ParameterConstructorWithParam_NewObjectWithParams()
    {
      //Given
      string[] strings = new string[] { "123", "qwe", "asd", "zxc" };

      //When
      ParameterConstructor obj = Make.A<ParameterConstructor>(new object[] { strings });

      //Then
      obj.strings.Should().BeEquivalentTo(strings);
    }
  }
}