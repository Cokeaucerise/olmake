using System;
using System.Reflection;
using FluentAssertions;
using Xunit;

namespace OLMake.Tests
{
	public partial class ExtensionsTests
	{
		[Fact]
		public void Fill_PropertyNameOnly_RandomValueInProperty()
		{
			//Given
			var tc = new TestClass();

			//When
			tc.Fill<TestClass, string>("StringProp");

			//Then
			tc.StringProp.Should().NotBeNull();
		}

		[Fact]
		public void Fill_PropertyNameWithValue_ValueInProperty()
		{
			//Given
			TestClass tc = new TestClass();
			string stringValue = "TestString";

			//When
			tc.Fill("StringProp", stringValue);

			//Then
			tc.StringProp.Should().NotBeNull();
			tc.StringProp.Should().Be(stringValue);
		}

		[Fact]
		public void Fill_PrivatePropertyNameWithValue_ValueInProperty()
		{
			//Given
			TestClass tc = new TestClass();
			string privatePropertyName = "PrivateStringProp";
			string stringValue = "TestString";

			//When
			tc.Fill(privatePropertyName, stringValue);

			//Then
			string propValue = (string) tc.GetType().GetTypeInfo().GetDeclaredProperty(privatePropertyName).GetValue(tc);
			propValue.Should().NotBeNull();
			propValue.Should().Be(stringValue);
		}

		[Fact]
		public void Fill_PropertyNameWithFiller_FillerValueInProperty()
		{
			//Given
			TestClass tc = new TestClass();
			string stringValue = "TestString";
			Func<string> stringFiller = () => stringValue;

			//When
			tc.Fill("StringProp", stringFiller);

			//Then
			tc.StringProp.Should().NotBeNull();
			tc.StringProp.Should().Be(stringValue);
		}

		[Fact]
		public void Fill_PropertyNameWithInvalidValueTypeExplicit_ThrowArgumentException()
		{
			//Given
			TestClass tc = new TestClass();

			//When

			//Then
			Assert.Throws(new ArgumentException().GetType(), () => tc.Fill<TestClass, bool>("StringProp"));
		}

		[Fact]
		public void Fill_PropertyNameWithInvalidValueTypeImplicit_ThrowArgumentException()
		{
			//Given
			TestClass tc = new TestClass();

			//When

			//Then
			Assert.Throws(new ArgumentException().GetType(), () => tc.Fill("StringProp", true));
		}

		[Fact]
		public void Fill_PropertyExpression_RandomValueInProperty()
		{
			//Given
			TestClass tc = new TestClass();

			//When
			tc.Fill(obj => obj.StringProp);

			//Then
			tc.StringProp.Should().NotBeNull();
		}

		[Fact]
		public void Fill_PropertyExpressionWithValue_ValueInProperty()
		{
			//Given
			TestClass tc = new TestClass();
			string stringValue = "TestString";

			//When
			tc.Fill(obj => obj.StringProp, stringValue);

			//Then
			tc.StringProp.Should().NotBeNull();
			tc.StringProp.Should().Be(stringValue);
		}

		[Fact]
		public void Fill_PropertyExpression_FillerValueInProperty()
		{
			//Given
			TestClass tc = new TestClass();
			string stringValue = "TestString";
			Func<string> stringFiller = () => stringValue;

			//When
			tc.Fill(obj => obj.StringProp, stringFiller);

			//Then
			tc.StringProp.Should().NotBeNull();
			tc.StringProp.Should().Be(stringValue);
		}
	}
}