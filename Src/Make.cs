using System;
using System.Collections.Generic;
using OLMake.Transposer;

namespace OLMake
{
	public static class Make
	{
		private static readonly ITransposer _transposer = new Transposer.Transposer();

		/// <summary>
		/// Generate an instance of the specified type filled with random values in the writable fields
		/// </summary>
		/// <typeparam name="T">The target object type</typeparam>
		/// <param name="param">Constructor parameters</param>
		/// <returns>New instance of type T</returns>
		public static T A<T>(params object[] param)
		{
			return _transposer.Fill<T>(param);
		}

		/// <summary>
		/// Generate a collection of the specified type filled with random values in the writable fields
		/// </summary>
		/// <typeparam name="T">The target object type</typeparam>
		/// <returns>New collection of type T</returns>
		public static IEnumerable<T> AList<T>()
		{
			return AList<T>(_transposer.DefaultCollectionSize);
		}

		/// <summary>
		/// Generate a collection of the specified type filled with random values in the writable fields
		/// </summary>
		/// <typeparam name="T">The target object type</typeparam>
		/// <param name="count">Count of the number of elements in the new collection</param>
		/// <returns>New collection of type T</returns>
		public static IEnumerable<T> AList<T>(int count)
		{
			return AList<T>(count, () => A<T>());
		}

		/// <summary>
		/// Generate a collection of the specified type filled with random values in the writable fields
		/// </summary>
		/// <typeparam name="T">The target object type</typeparam>
		/// <param name="factory">Function called to generate the new instances in the collection</param>
		/// <returns>New collection of type T</returns>
		public static IEnumerable<T> AList<T>(Func<T> factory)
		{
			return AList<T>(_transposer.DefaultCollectionSize, factory);
		}

		/// <summary>
		/// Generate a collection of the specified type filled with random values in the writable fields
		/// </summary>
		/// <typeparam name="T">The target object type</typeparam>
		/// <param name="count">Count of the number of elements in the new collection</param>
		/// <param name="factory">Function called to generate the new instances in the collection</param>
		/// <returns>New collection of type T</returns>
		public static IEnumerable<T> AList<T>(int count, Func<T> factory)
		{
			return _transposer.FillCollection(count, factory);
		}

		/// <summary>
		/// Generate a single word
		/// </summary>
		/// <returns>Word string</returns>
		public static string AWord()
		{
			return _transposer.GetWord();
		}

		/// <summary>
		/// Generate a sentence that start with a capital letter and end's with a periode.
		/// Contain between 8 and 16 words.
		/// </summary>
		/// <returns>Sentence string</returns>
		public static string ASentence()
		{
			return _transposer.GetSentence();
		}

		/// <summary>
		/// Generate a paragraph that contains between 5 and 10 sentences.
		/// </summary>
		/// <returns>Paragraph string</returns>
		public static string AParagraph()
		{
			return _transposer.GetParagraph();
		}

		/// <summary>
		/// Generate a text that contains between 3 and 6 paragraph separeted with new lines.
		/// </summary>
		/// <returns>Text string</returns>
		public static string AText()
		{
			return _transposer.GetText();
		}
	}
}