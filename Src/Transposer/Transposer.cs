using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using OLMake.Generators;

namespace OLMake.Transposer
{
	internal interface ITransposer
	{
		int DefaultCollectionSize { get; }
		T Fill<T>(params object[] param);
		IEnumerable<T> FillCollection<T>(int count, Func<T> factory);
		string GetWord();
		string GetSentence();
		string GetParagraph();
		string GetText();
	}

	internal class Transposer : ITransposer
	{
		//===CONSTANTS===//
		private const int DEFAULT_COLLECTION_SIZE = 25;
		private const int WORDS_MIN_COUNT = 8;
		private const int WORDS_MAX_COUNT = 16;
		private const int SENTENCES_MIN_COUNT = 5;
		private const int SENTENCES_MAX_COUNT = 10;
		private const int PARAGRAPH_MIN_COUNT = 3;
		private const int PARAGRAPH_MAX_COUNT = 6;

		//===PRIVATE MEMBERS===//
		private readonly IGenerator _generator = new Generator();

		//===PUBLIC PROPERTIES===//
		public int DefaultCollectionSize { get { return DEFAULT_COLLECTION_SIZE; } }

		//===METHODS==//
		public T Fill<T>(params object[] param)
		{
			return (T) Fill(typeof(T), param);
		}

		private object Fill(Type type, params object[] param)
		{
			//No try catch arround to throw MissingMethodException if type requires params on top level instanciation
			return FillObject(type, _generator.Generate(type, param));
		}

		private Object FillObject(Type type, Object instance)
		{
			if (!type.IsBaseType() && instance != null)
			{
				foreach (FieldInfo fi in type.WriteFields())
				{
					//Ignore already set fields from constructor
					if (!fi.FieldType.IsValueType && fi.GetValue(instance) != null) continue;

					//return null value for inner object that requires complex instanciation (no parameterless constructor).
					object val = null;
					try
					{
						val = _generator.Generate(fi.FieldType);
					}
					catch (MissingMethodException)
					{
						return null;
					}

					//Recursively set the values of the inner WriteFields;
					fi.SetValue(instance, FillObject(fi.FieldType, val));
				}
			}

			return instance;
		}

		public IEnumerable<T> FillCollection<T>(int count, Func<T> factory)
		{
			List<T> list = new List<T>();
			for (int i = 0; i < count; i++)
			{
				list.Add(factory());
			}
			return list;
		}

		public string GetWord()
		{
			return _generator.Generate<string>();
		}

		public string GetSentence()
		{
			string result = "";
			int count = new Random().Next(WORDS_MIN_COUNT, WORDS_MAX_COUNT + 1);
			for (int i = 0; i < count; i++)
			{
				result += $" {GetWord()}";
			}

			//Remove the starting space and replace the first letter with Capital letter
			result = result.TrimStart(' ');
			string firstLetter = result.First().ToString();
			result = firstLetter.ToUpperInvariant() + result.Substring(1, result.Length - 1);

			//Append periode at the end
			return $"{result}.";
		}

		public string GetParagraph()
		{
			string result = "";
			int count = new Random().Next(SENTENCES_MIN_COUNT, SENTENCES_MAX_COUNT + 1);
			for (int i = 0; i < count; i++)
			{
				//Place space between each sentences
				result += $" {GetSentence()}";
			}

			//Remove first space
			return result.TrimStart(' ');
		}

		public string GetText()
		{
			string result = "";
			int count = new Random().Next(PARAGRAPH_MIN_COUNT, PARAGRAPH_MAX_COUNT + 1);
			for (int i = 0; i < count; i++)
			{
				//Place new lines between each paragraph
				result += $"{GetParagraph()}\r\n\n";
			}

			//Remove trailing new lines and cariage return
			return result.TrimEnd('\r', '\n');
		}
	}

	//===TYPE EXTENSIONS===//
	internal static class TypeExtension
	{
		/// <summary>
		/// Rerturn the declared fields of the type that are writables (not const, readonly, { get; }, interface)
		/// </summary>
		/// <param name="type">Target type</param>
		/// <returns>Collection of FieldInfo</returns>
		public static IEnumerable<FieldInfo> WriteFields(this Type type)
		{
			var baseTypeFields = type.GetTypeInfo().BaseType?.GetTypeInfo().DeclaredFields.WritableFields();
			var typeFields = type.GetTypeInfo().DeclaredFields.WritableFields();
			var result = baseTypeFields == null ? typeFields : typeFields.Concat(baseTypeFields);
			return result;
		}

		private static IEnumerable<FieldInfo> WritableFields(this IEnumerable<FieldInfo> fields)
		{
			return fields.Where(fi => !fi.IsLiteral && !fi.IsInitOnly && !fi.FieldType.GetTypeInfo().IsInterface);
		}

		/// <summary>
		/// Check if the type is a base type (all primary types plus decimal and string)
		/// </summary>
		/// <param name="type">Target type</param>
		/// <returns>boolean</returns>
		public static bool IsBaseType(this Type type)
		{
			return type.GetTypeInfo().IsPrimitive || type == typeof(Decimal) || type == typeof(string);
		}

	}
}