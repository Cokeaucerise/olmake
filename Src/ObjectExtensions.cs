using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace OLMake
{
	/// <summary>
	/// Collection of extension method that can be used to simply place value inside specific property field
	/// </summary>
	public static class ObjectExtensions
	{
		/// <summary>
		/// Fill the specified property with the default random value for the type
		/// </summary>
		/// <typeparam name="TClass">The target object type</typeparam>
		/// <typeparam name="TProperty">The target property type</typeparam>
		/// <param name="propertyName">The name of the target property</param>
		/// <returns>A configurator for the target object type</returns>
		public static TClass Fill<TClass, TProperty>(this TClass obj, string propertyName)
		{
			return obj.Fill(propertyName, () => Make.A<TProperty>());
		}

		/// <summary>
		/// Fill the specified property with the specified value
		/// </summary>
		/// <typeparam name="TClass">The target object type</typeparam>
		/// <typeparam name="TProperty">The target property type</typeparam>
		/// <param name="propertyName">The name of the target property</param>
		/// <param name="value">A property value</param>
		/// <returns>A configurator for the target object type</returns>
		public static TClass Fill<TClass, TProperty>(this TClass obj, string propertyName, TProperty value)
		{
			return obj.Fill(propertyName, () => value);
		}

		/// <summary>
		/// Fill the specified property with the result of the specified function
		/// </summary>
		/// <typeparam name="TClass">The target object type</typeparam>
		/// <typeparam name="TProperty">The target property type</typeparam>
		/// <param name="propertyName">The name of the target property</param>
		/// <param name="filler">A function that will return a property value</param>
		/// <returns>A configurator for the target object type</returns>
		public static TClass Fill<TClass, TProperty>(this TClass obj, string propertyName, Func<TProperty> filler)
		{
			obj.GetType().GetTypeInfo().GetDeclaredProperty(propertyName).SetValue(obj, filler());
			return obj;
		}

		/// <summary>
		/// Fill the specified property with default random value for the type
		/// </summary>
		/// <typeparam name="TClass">The target object type</typeparam>
		/// <typeparam name="TProperty">The target property type</typeparam>
		/// <param name="expression">The target property</param>
		/// <returns>A configurator for the target object type</returns>
		public static TClass Fill<TClass, TProperty>(this TClass obj, Expression<Func<TClass, TProperty>> expression)
		{
			return obj.Fill(expression, Make.A<TProperty>());
		}

		/// <summary>
		/// Fill the specified property with the specified value
		/// </summary>
		/// <typeparam name="TClass">The target object type</typeparam>
		/// <typeparam name="TProperty">The target property type</typeparam>
		/// <param name="expression">The target property</param>
		/// <param name="value">A property value</param>
		/// <returns>A configurator for the target object type</returns>
		public static TClass Fill<TClass, TProperty>(this TClass obj, Expression<Func<TClass, TProperty>> expression, TProperty value)
		{
			return obj.Fill(expression, () => value);
		}

		/// <summary>
		/// Fill the specified property with the result of the specified function
		/// </summary>
		/// <typeparam name="TClass">The target object type</typeparam>
		/// <typeparam name="TProperty">The target property type</typeparam>
		/// <param name="expression">The target property</param>
		/// <param name="filler">A function that will return a property value</param>
		/// <returns>A configurator for the target object type</returns>
		public static TClass Fill<TClass, TProperty>(this TClass obj, Expression<Func<TClass, TProperty>> expression, Func<TProperty> filler)
		{
			PropertyInfo propertyInfo = (expression.Body as MemberExpression).Member as PropertyInfo;
			propertyInfo.SetValue(obj, filler());
			return obj;
		}

		public static Task<T> ToTask<T>(this T value)
		{
			return Task.FromResult(value);
		}
	}
}