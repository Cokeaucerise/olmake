using System;

namespace OLMake.Generators
{
	internal interface INumberGenerator
	{
		Boolean NextBoolean();
		Byte NextByte();
		Int16 NextInt16();
		Int32 NextInt32();
		Int64 NextInt64();
		IntPtr NextIntPtr();
		SByte NextSByte();
		UInt16 NextUInt16();
		UInt32 NextUInt32();
		UInt64 NextUInt64();
		UIntPtr NextUIntPtr();
		float NextFloat();
		double NextDouble();
		decimal NextDecimal();
	}

	/// <summary>
	/// Random number generator for every primary numerical types.
	/// </summary>
	internal class NumberGenerator : INumberGenerator
	{
		//===PRIVATE MEMBERS===//
		private readonly Random r;

		//===CONSTRUCTORS===//
		public NumberGenerator(Random random)
		{
			this.r = random;
		}

		//===METHODS===//
		public Boolean NextBoolean() => r.Next(2) == 1;
		public Byte NextByte() => (Byte) r.Next(Byte.MinValue, Byte.MaxValue + 1);
		public SByte NextSByte() => (SByte) r.Next(SByte.MinValue, SByte.MaxValue + 1);
		public Int16 NextInt16() => (Int16) r.Next(Int16.MinValue, Int16.MaxValue + 1);
		public Int32 NextInt32() => (Int32) NextInt16() << 16 | (Int32) NextInt16();
		public Int64 NextInt64() => (Int64) NextInt32() << 32 | (Int64) NextInt32();
		public IntPtr NextIntPtr() => new IntPtr(NextInt64());
		public UInt16 NextUInt16() => (UInt16) r.Next(UInt16.MinValue, UInt16.MaxValue + 1);
		public UInt32 NextUInt32() => (UInt32) NextUInt16() << 16 | (UInt32) NextUInt16();
		public UInt64 NextUInt64() => (UInt64) NextUInt32() << 32 | (UInt64) NextUInt32();
		public UIntPtr NextUIntPtr() => new UIntPtr(NextUInt64());
		public float NextFloat() => (float) NextDouble();
		public double NextDouble() => r.NextDouble();

		public decimal NextDecimal()
		{
			byte scale = (byte) r.Next(29);
			bool sign = r.Next(2) == 1;
			return new decimal(NextInt32(), NextInt32(), NextInt32(), sign, scale);
		}
	}
}