using System;
using System.Linq;

namespace OLMake.Generators
{
	internal interface IGenerator
	{
		/// <summary>
		/// Return a new instance with random values for base types and an empty instance for other objects
		/// </summary>
		/// <typeparam name="T">Target type</typeparam>
		/// <param name="param">contructor parameters</param>
		/// <returns>Random values</returns>
		T Generate<T>(params object[] param) where T : class;

		/// <summary>
		/// Return a new instance with random values for base types and an empty instance for other objects
		/// </summary>
		/// <param name="type">Target type</param>
		/// <param name="param">contructor parameters</param>
		/// <returns>Random values</returns>
		Object Generate(Type type, params object[] param);
	}

	/// <summary>
	/// Combine the literal and numerical generators
	/// </summary>
	internal class Generator : IGenerator
	{
		//===PRIVATE MEMBERS===//
		private static readonly Random r = new Random(48); // 48 for the A-48.
		private readonly INumberGenerator _numberGenerator;
		private readonly IWordGenerator _wordGenerator;

		//===CONSTRUCTORS===//
		public Generator()
		{
			this._numberGenerator = new NumberGenerator(r);
			this._wordGenerator = new WordGenerator(r);
		}

		//===METHODS===//
		/// <summary>
		/// Return a new instance with random values for base types and an empty instance for other objects
		/// </summary>
		/// <typeparam name="T">Target type</typeparam>
		/// <param name="param">contructor parameters</param>
		/// <returns>Random values</returns>
		public T Generate<T>(params object[] param)
		where T : class
		{
			return Generate(typeof(T), param) as T;
		}

		/// <summary>
		/// Return a new instance with random values for base types and an empty instance for other objects
		/// </summary>
		/// <param name="type">Target type</param>
		/// <param name="param">contructor parameters</param>
		/// <returns>Random values</returns>
		public Object Generate(Type type, params object[] param)
		{
			if (type == typeof(char)) return _wordGenerator.NextChar();
			else if (type == typeof(string)) return _wordGenerator.NextString();
			else if (type == typeof(Boolean)) return _numberGenerator.NextBoolean();
			else if (type == typeof(Byte)) return _numberGenerator.NextByte();
			else if (type == typeof(Int16)) return _numberGenerator.NextInt16();
			else if (type == typeof(Int32)) return _numberGenerator.NextInt32();
			else if (type == typeof(Int64)) return _numberGenerator.NextInt64();
			else if (type == typeof(IntPtr)) return _numberGenerator.NextIntPtr();
			else if (type == typeof(SByte)) return _numberGenerator.NextSByte();
			else if (type == typeof(UInt16)) return _numberGenerator.NextUInt16();
			else if (type == typeof(UInt32)) return _numberGenerator.NextUInt32();
			else if (type == typeof(UInt64)) return _numberGenerator.NextUInt64();
			else if (type == typeof(UIntPtr)) return _numberGenerator.NextUIntPtr();
			else if (type == typeof(float)) return _numberGenerator.NextFloat();
			else if (type == typeof(double)) return _numberGenerator.NextDouble();
			else if (type == typeof(decimal)) return _numberGenerator.NextDecimal();
			else
			{
				var ctr = type.GetConstructor(param.Select(p => p.GetType()).ToArray());
				if (ctr != null)
					return ctr.Invoke(param);
				else
					return null;
			}
		}
	}
}